#include <stdio.h>

int main()
{
  int i, num;

  printf("Enter a number to find factors=\n");
  scanf("%d", &num);

  printf("\n Factors of the Given Number are:\n");
  for (i = 1; i <= num;i++)
   {
     if(num%i == 0)
        {
		 printf(" %d",i);
		}
   }

  return 0;
}

